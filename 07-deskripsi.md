Q: Sebutkan dan jelaskan Jenis-jenis IP beserta kelas-kelasnya yang kamu ketahui (gambarkan ke dalam bentuk tabel)! MAX SCORE: 10

## Versi IP Address
### IPv4
Ip address yang karakteristiknya adalah memiliki panjang angka 32 bit dan terdiri dari empat kumpulan angka yang dipisahkan oleh titik. Masing-masing kumpulan angka tersebut adalah representasi desimal dari delapan digit (bit) angka biner. ex: `172.16.254.1`

### IPv6
IPv6 memiliki panjang angka 128 bit dan terdiri dari delapan kumpulan angka dan huruf yang dipisahkan oleh titik dua. Masing-masing kumpulan tersebut merupakan representasi desimal dari 16 angka biner. ex: `40.282.366.920.938.463.463.374.607.431.768.211.456`

## Jenis IP Address
### IP Publik
Ip adress yang dapat diakses melalui jaringan internet yang menghubungkan internet (dunia luar) ke gateway di local network kita.
### IP Privat
Ip address yang ter-attached ke setiap device kita di LOCAL AREA NETWORK kita. yang mana address tersebut hanya dapat diakses di LAN kita saja.

### IP Dinamis dan IP Statis
Ip dinamis = Ip address yang berubah-ubah contoh : koneksi putus, dan re-koneksi
Ip statis = reserved ip address yang tidak akan berubah, sehingga konsisten dan tidak berubah selama tetap dalam provider atau server yang sama


### Shared IP dan Dedicated IP
shared ip = shared IP biasanya terdapat pada server shared hosting, di mana semua penggunanya berbagi seluruh sumber daya server tersebut, termasuk IP address-nya. Tak hanya itu, semua domain milik seorang pengguna juga memakai alamat IP yang sama.
dedicated ip = Jika shared IP dipakai bersama-sama oleh semua pengguna pada suatu server, dedicated IP hanya digunakan oleh satu domain.

Meskipun jenis IP address ini umumnya ditawarkan pada server dedicated hosting dan cloud VPS hosting, beberapa penyedia layanan web hosting memperbolehkan pelanggannya untuk menggunakan dedicated IP pada server shared hosting.

## Kelas IP Address
IP address IPv4 juga dibagi menjadi beberapa kelas. Masing-masing memiliki rentang angka serta jumlah maksimal alamat IP dan jaringan:

Kelas A
Rentang angka : 0.0.0.0 – 127.255.255.255
Jumlah maksimal alamat IP : 16.777.216
Jumlah maksimal jaringan : 128
Kelas B
Rentang angka : 128.0.0.0 – 191.255.255.255
Jumlah maksimal alamat IP : 1.048.576
Jumlah maksimal jaringan : 16.384
Kelas C
Rentang angka : 192.0.0.0 – 223.255.255.255
Jumlah maksimal alamat IP : 65.536
Jumlah maksimal jaringan : 2.097.152
Kelas D
Rentang angka : 224.0.0.0 – 239.255.255.255
Jumlah maksimal alamat IP : tidak didefinisikan
Jumlah maksimal jaringan : tidak didefinisikan
Kelas E
Rentang angka : 140.0.0.0 – 255.255.255.255
Jumlah maksimal alamat IP : tidak didefinisikan
Jumlah maksimal jaringan : tidak didefinisikan

Q: CI/CD (Continuous Integration/Continuous Development) merupakan hal yang sering ditemui oleh seorang DevOps. Menurut pendapatmu mengapa CI/CD diperlukan, dan berikan gambaran diagram kerjanya (Workflow) ?! MAX SCORE: 10

A: dizaman dimana kegagalan dan learn fast dalah kunci dalam bisnis, maka kemampuan untuk build->test->deploy sebuah aplikasi menjadi krusial. terlebih lagi kemudahan dan otomatisasi yang didapat dari CI/CD yang membuat proses build sampai deployment menjadi mudah, cepat dan murah.
dan juga dengan CI/CD memungkinkan bisnis untuk mencapai level 0 downtime system, sehingga akan memberikan dampak luar biasa bagi customernya.

untuk alur , saya rasa terjawab dengan `02-diagram.png`